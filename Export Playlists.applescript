(*

Export Playlists Multi
Petros Loukareas (PEL) 180802 v2

Dan Petrescu's Export contained a lot of great ideas, but was
buggy when used with CJK titles and too complicated for my needs,
so I thought I'd redo a simpler version. Use as you see fit, but
please acknowledge me in derived works.

NEW IN v2: 

- ProgBar is no longer needed now. Please compile as app bundle
to make progress window visible.

- The 'Renumber' button (when asked if songs should be
renumbered to maintain the order of the playlist) is no longer
the default.


Known issues and limitations: 

- Since the playlists are looked up by name, the
problem of identically named playlists is resolved by picking the
first one that it stumbles upon, therefore in such cases it might
not export the playlist you were expecting or even the one
you selected (if you'd have any way of knowing which one you
picked). If you need to export such a playlist, consider renaming
it temporarily.

- If the name of the playlist begins with a dot ('.'), the resulting
folder will have an added underscore ("_") before it.

- Since it uses the Finder's copy command, you might want to turn
down the volume for the duration of the script. :)

*)

global errorNum
global folderPath
global folderName
global folderNameBase
global currentMusicFolderPath
global retainPlaylistOrder

set folderNameBase to "iTunes Export "
set currentMusicFolderPath to ""
set retainPlaylistOrder to false

set warningNoSelection to "No playlists were selected - exiting."
set warningTooManyFolders to "Ten folders have already been generated on your desktop. Please clean up or delete some of these, and run the script again."
set warningTooManySameNameFolders to "There are more than ten playlists selected that have the same name. Try renaming some of them and run the script again."
set warningSomethingElseWentWrong to "Sorry, something went wrong (probably an illegal character in a folder name). Please contact the developer with details."

tell application "iTunes"
	set m to (get name of every user playlist whose special kind is none)
	set n to (choose from list m with prompt ("Choose which playlists to export (you can choose multiple out of " & ((count of m) as string) & " regular and smart playlists.)") with title "Choose Playlist for Export" with multiple selections allowed)
end tell

if n is false then
	popUpWarning(warningNoSelection)
	return
end if

tell me to activate
set b to button returned of (display dialog "Would you like to have the playlist files renumbered on copy? This will retain the order of the files as they were in the playlist, and will preserve copy of files with equal file names." with title "Renumber playlist items?" buttons {"Renumber", "Don't renumber"} default button "Don't renumber")
if b is "Renumber" then set retainPlaylistOrder to true

getFolderPath()

set errNo to makeFolder()
if errNo > 0 then
	popUpWarning(warningTooManyFolders)
	return
end if

set progress total steps to count of n
set progress completed steps to 0
set progress description to "Exporting Playlists..."
set progress additional description to "Preparing export..."

set c to 1
repeat with i in n
	set progress additional description to "Processing playlist " & c & " of " & (count of n)
	set errNo to makeFolderInFolder(i)
	if errNo > 0 then
		set progress total steps to 0
		set progress completed steps to 0
		set progress description to ""
		set progress additional description to ""
		if errNo is 1 then
			popUpWarning(warningTooManySameNameFolders)
		else
			popUpWarning(warningSomethingElseWentWrong)
		end if
		return
	end if
	tell application "iTunes"
		set t to tracks of user playlist i
		set ap to AppleScript's text item delimiters
		set AppleScript's text item delimiters to ":"
		set s to 1
		repeat with j in t
			set y to (get location of j)
			tell application "Finder"
				try
					copy file y to folder currentMusicFolderPath
				on error
					-- do nothing, just skip copying
				end try
				if retainPlaylistOrder is true then
					set z to text items of (y as text)
					set f to last item of z
					set g to my getCountString(s) & "_" & f
					tell application "System Events" to set name of file (currentMusicFolderPath & ":" & f) to g
				end if
			end tell
			set s to s + 1
		end repeat
		set AppleScript's text item delimiters to ap
	end tell
	set progress completed steps to c
	set c to c + 1
end repeat

set progress completed steps to c
delay 0.5
set progress total steps to 0
set progress completed steps to 0
set progress description to ""
set progress additional description to ""
set r to count of n
if r > 0 then
	if r is 1 then
		set q to " playlist was exported."
	else
		set q to " playlists were exported."
	end if
	display notification ((count of n) as string) & q with title "All Done"
else
	display notification "No playlists were exported." with title "All Done...?"
end if

on popUpWarning(argument)
	tell me to activate
	display dialog argument buttons {"OK"} default button "OK"
end popUpWarning

on getFolderPath()
	tell application "Finder"
		set sD to the name of the startup disk
	end tell
	tell application "System Events"
		set uN to the name of the current user
	end tell
	set folderPath to sD & ":Users:" & uN & ":Desktop:"
end getFolderPath

on makeFolder()
	tell application "Finder"
		set folderName to folderNameBase & short date string of (current date)
		set appendString to ""
		set appendCount to 0
		repeat while appendCount < 10
			try
				make new folder at folderPath with properties {name:folderName & appendString}
				set folderName to folderName & appendString
				return 0
				exit repeat
			on error
				set appendCount to appendCount + 1
				set appendString to (" [" & appendCount as text) & "]"
			end try
		end repeat
		if appendCount is 10 then
			set errorNum to 1
			return errorNum
		else
			set errorNum to 0
			return errorNum
		end if
	end tell
end makeFolder

on makeFolderInFolder(nameOfFolder)
	if (get text 1 thru 1 of nameOfFolder) is "." then set nameOfFolder to "_" & nameOfFolder
	tell application "Finder"
		set musicFolderName to nameOfFolder
		set exportFolderPath to folderPath & folderName
		set appendString to ""
		set appendCount to 0
		repeat while appendCount < 10
			try
				make new folder at exportFolderPath with properties {name:musicFolderName & appendString}
				set musicFolderName to musicFolderName & appendString
				set currentMusicFolderPath to exportFolderPath & ":" & musicFolderName
				return 0
				exit repeat
			on error
				set appendCount to appendCount + 1
				set appendString to (" [" & appendCount as text) & "]"
			end try
		end repeat
		if appendCount is 10 then
			set errorNum to 1
			return errorNum
		else
			set errorNum to 0
			return errorNum
		end if
	end tell
end makeFolderInFolder

on getCountString(theNumber)
	set cString to theNumber as text
	set c to length of cString
	if c < 4 then
		return (get text 1 thru (4 - c) of "0000") & cString
	else
		return cString
	end if
end getCountString