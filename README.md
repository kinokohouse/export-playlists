# Export Playlist #
A simple script to select and export playlists (that is, the files the playlists are made up of) from iTunes, keeping them in their respective folders. A hugely dumbed-down version of Dan Petrescu's original script; please see the comments at the top of the source for more information. This script does *NOT* export .m3u files. Export as application bundle from Script Editor to make sure the progress indicator shows up when the script is running.

The .applescript file is the raw source as text (cut-and-pasteable), the .scpt is the compiled and ready to use script. There is an exported application in the Downloads section to drop in your scripts folder.

Current version: v2 (180802). 

Use-as-you-see-fit license; again see the comments at the top of the source code.