FasdUAS 1.101.10   ��   ��    k             l      ��  ��   ��

Export Playlists Multi
Petros Loukareas (PEL) 180802 v2

Dan Petrescu's Export contained a lot of great ideas, but was
buggy when used with CJK titles and too complicated for my needs,
so I thought I'd redo a simpler version. Use as you see fit, but
please acknowledge me in derived works.

NEW IN v2: 

- ProgBar is no longer needed now. Please compile as app bundle
to make progress window visible.

- The 'Renumber' button (when asked if songs should be
renumbered to maintain the order of the playlist) is no longer
the default.


Known issues and limitations: 

- Since the playlists are looked up by name, the
problem of identically named playlists is resolved by picking the
first one that it stumbles upon, therefore in such cases it might
not export the playlist you were expecting or even the one
you selected (if you'd have any way of knowing which one you
picked). If you need to export such a playlist, consider renaming
it temporarily.

- If the name of the playlist begins with a dot ('.'), the resulting
folder will have an added underscore ("_") before it.

- Since it uses the Finder's copy command, you might want to turn
down the volume for the duration of the script. :)

     � 	 		V 
 
 E x p o r t   P l a y l i s t s   M u l t i 
 P e t r o s   L o u k a r e a s   ( P E L )   1 8 0 8 0 2   v 2 
 
 D a n   P e t r e s c u ' s   E x p o r t   c o n t a i n e d   a   l o t   o f   g r e a t   i d e a s ,   b u t   w a s 
 b u g g y   w h e n   u s e d   w i t h   C J K   t i t l e s   a n d   t o o   c o m p l i c a t e d   f o r   m y   n e e d s , 
 s o   I   t h o u g h t   I ' d   r e d o   a   s i m p l e r   v e r s i o n .   U s e   a s   y o u   s e e   f i t ,   b u t 
 p l e a s e   a c k n o w l e d g e   m e   i n   d e r i v e d   w o r k s . 
 
 N E W   I N   v 2 :   
 
 -   P r o g B a r   i s   n o   l o n g e r   n e e d e d   n o w .   P l e a s e   c o m p i l e   a s   a p p   b u n d l e 
 t o   m a k e   p r o g r e s s   w i n d o w   v i s i b l e . 
 
 -   T h e   ' R e n u m b e r '   b u t t o n   ( w h e n   a s k e d   i f   s o n g s   s h o u l d   b e 
 r e n u m b e r e d   t o   m a i n t a i n   t h e   o r d e r   o f   t h e   p l a y l i s t )   i s   n o   l o n g e r 
 t h e   d e f a u l t . 
 
 
 K n o w n   i s s u e s   a n d   l i m i t a t i o n s :   
 
 -   S i n c e   t h e   p l a y l i s t s   a r e   l o o k e d   u p   b y   n a m e ,   t h e 
 p r o b l e m   o f   i d e n t i c a l l y   n a m e d   p l a y l i s t s   i s   r e s o l v e d   b y   p i c k i n g   t h e 
 f i r s t   o n e   t h a t   i t   s t u m b l e s   u p o n ,   t h e r e f o r e   i n   s u c h   c a s e s   i t   m i g h t 
 n o t   e x p o r t   t h e   p l a y l i s t   y o u   w e r e   e x p e c t i n g   o r   e v e n   t h e   o n e 
 y o u   s e l e c t e d   ( i f   y o u ' d   h a v e   a n y   w a y   o f   k n o w i n g   w h i c h   o n e   y o u 
 p i c k e d ) .   I f   y o u   n e e d   t o   e x p o r t   s u c h   a   p l a y l i s t ,   c o n s i d e r   r e n a m i n g 
 i t   t e m p o r a r i l y . 
 
 -   I f   t h e   n a m e   o f   t h e   p l a y l i s t   b e g i n s   w i t h   a   d o t   ( ' . ' ) ,   t h e   r e s u l t i n g 
 f o l d e r   w i l l   h a v e   a n   a d d e d   u n d e r s c o r e   ( " _ " )   b e f o r e   i t . 
 
 -   S i n c e   i t   u s e s   t h e   F i n d e r ' s   c o p y   c o m m a n d ,   y o u   m i g h t   w a n t   t o   t u r n 
 d o w n   t h e   v o l u m e   f o r   t h e   d u r a t i o n   o f   t h e   s c r i p t .   : ) 
 
   
  
 l     ��������  ��  ��        p         ������ 0 errornum errorNum��        p         ������ 0 
folderpath 
folderPath��        p         ������ 0 
foldername 
folderName��        p         ������  0 foldernamebase folderNameBase��        p         ������ 00 currentmusicfolderpath currentMusicFolderPath��        p         ������ *0 retainplaylistorder retainPlaylistOrder��        l     ��������  ��  ��       !   l     "���� " r      # $ # m      % % � & &  i T u n e s   E x p o r t   $ o      ����  0 foldernamebase folderNameBase��  ��   !  ' ( ' l    )���� ) r     * + * m     , , � - -   + o      ���� 00 currentmusicfolderpath currentMusicFolderPath��  ��   (  . / . l    0���� 0 r     1 2 1 m    	��
�� boovfals 2 o      ���� *0 retainplaylistorder retainPlaylistOrder��  ��   /  3 4 3 l     ��������  ��  ��   4  5 6 5 l    7���� 7 r     8 9 8 m     : : � ; ; J N o   p l a y l i s t s   w e r e   s e l e c t e d   -   e x i t i n g . 9 o      ���� (0 warningnoselection warningNoSelection��  ��   6  < = < l    >���� > r     ? @ ? m     A A � B B � T e n   f o l d e r s   h a v e   a l r e a d y   b e e n   g e n e r a t e d   o n   y o u r   d e s k t o p .   P l e a s e   c l e a n   u p   o r   d e l e t e   s o m e   o f   t h e s e ,   a n d   r u n   t h e   s c r i p t   a g a i n . @ o      ���� .0 warningtoomanyfolders warningTooManyFolders��  ��   =  C D C l    E���� E r     F G F m     H H � I I � T h e r e   a r e   m o r e   t h a n   t e n   p l a y l i s t s   s e l e c t e d   t h a t   h a v e   t h e   s a m e   n a m e .   T r y   r e n a m i n g   s o m e   o f   t h e m   a n d   r u n   t h e   s c r i p t   a g a i n . G o      ���� >0 warningtoomanysamenamefolders warningTooManySameNameFolders��  ��   D  J K J l    L���� L r     M N M m     O O � P P � S o r r y ,   s o m e t h i n g   w e n t   w r o n g   ( p r o b a b l y   a n   i l l e g a l   c h a r a c t e r   i n   a   f o l d e r   n a m e ) .   P l e a s e   c o n t a c t   t h e   d e v e l o p e r   w i t h   d e t a i l s . N o      ���� >0 warningsomethingelsewentwrong warningSomethingElseWentWrong��  ��   K  Q R Q l     ��������  ��  ��   R  S T S l   h U���� U O    h V W V k     g X X  Y Z Y r     9 [ \ [ l    5 ]���� ] e     5 ^ ^ 6    5 _ ` _ n     % a b a 1   # %��
�� 
pnam b 2     #��
�� 
cUsP ` =  ( 3 c d c 1   ) -��
�� 
pSpK d m   . 2��
�� eSpKkNon��  ��   \ o      ���� 0 m   Z  e�� e r   : g f g f l  : c h���� h I  : c�� i j
�� .gtqpchltns    @   @ ns   i o   : =���� 0 m   j �� k l
�� 
prmp k l  @ S m���� m b   @ S n o n b   @ O p q p m   @ C r r � s s � C h o o s e   w h i c h   p l a y l i s t s   t o   e x p o r t   ( y o u   c a n   c h o o s e   m u l t i p l e   o u t   o f   q l  C N t���� t c   C N u v u l  C J w���� w I  C J�� x��
�� .corecnte****       **** x o   C F���� 0 m  ��  ��  ��   v m   J M��
�� 
TEXT��  ��   o m   O R y y � z z <   r e g u l a r   a n d   s m a r t   p l a y l i s t s . )��  ��   l �� { |
�� 
appr { m   V Y } } � ~ ~ 4 C h o o s e   P l a y l i s t   f o r   E x p o r t | �� ��
�� 
mlsl  m   \ ]��
�� boovtrue��  ��  ��   g o      ���� 0 n  ��   W m     � ��                                                                                  hook  alis    B  Sixtyten                   �W;�H+     {
iTunes.app                                                     �5�5�d�>        ����  	                Applications    �W�      �d�       {  !Sixtyten:Applications: iTunes.app    
 i T u n e s . a p p    S i x t y t e n  Applications/iTunes.app   / ��  ��  ��   T  � � � l     ��������  ��  ��   �  � � � l  i ~ ����� � Z   i ~ � ����� � =  i n � � � o   i l���� 0 n   � m   l m��
�� boovfals � k   q z � �  � � � I   q w�� ����� 0 popupwarning popUpWarning �  ��� � o   r s���� (0 warningnoselection warningNoSelection��  ��   �  ��� � L   x z����  ��  ��  ��  ��  ��   �  � � � l     ��������  ��  ��   �  � � � l   � ����� � O   � � � � I  � �������
�� .miscactvnull��� ��� null��  ��   �  f    ���  ��   �  � � � l  � � ����� � r   � � � � � n   � � � � � 1   � ���
�� 
bhit � l  � � ����� � I  � ��� � �
�� .sysodlogaskr        TEXT � m   � � � � � � �t W o u l d   y o u   l i k e   t o   h a v e   t h e   p l a y l i s t   f i l e s   r e n u m b e r e d   o n   c o p y ?   T h i s   w i l l   r e t a i n   t h e   o r d e r   o f   t h e   f i l e s   a s   t h e y   w e r e   i n   t h e   p l a y l i s t ,   a n d   w i l l   p r e s e r v e   c o p y   o f   f i l e s   w i t h   e q u a l   f i l e   n a m e s . � �� � �
�� 
appr � m   � � � � � � � 0 R e n u m b e r   p l a y l i s t   i t e m s ? � �� � �
�� 
btns � J   � � � �  � � � m   � � � � � � �  R e n u m b e r �  ��� � m   � � � � � � �  D o n ' t   r e n u m b e r��   � �� ���
�� 
dflt � m   � � � � � � �  D o n ' t   r e n u m b e r��  ��  ��   � o      ���� 0 b  ��  ��   �  � � � l  � � ����� � Z  � � � ����� � =  � � � � � o   � ����� 0 b   � m   � � � � � � �  R e n u m b e r � r   � � � � � m   � ���
�� boovtrue � o      ���� *0 retainplaylistorder retainPlaylistOrder��  ��  ��  ��   �  � � � l     ��������  ��  ��   �  � � � l  � � ����� � I   � ��������� 0 getfolderpath getFolderPath��  ��  ��  ��   �  � � � l     ��~�}�  �~  �}   �  � � � l  � � ��|�{ � r   � � � � � I   � ��z�y�x�z 0 
makefolder 
makeFolder�y  �x   � o      �w�w 0 errno errNo�|  �{   �  � � � l  � � ��v�u � Z   � � � ��t�s � ?   � � � � � o   � ��r�r 0 errno errNo � m   � ��q�q   � k   � � � �  � � � I   � ��p ��o�p 0 popupwarning popUpWarning �  ��n � o   � ��m�m .0 warningtoomanyfolders warningTooManyFolders�n  �o   �  ��l � L   � ��k�k  �l  �t  �s  �v  �u   �  � � � l     �j�i�h�j  �i  �h   �  � � � l  � � ��g�f � r   � � � � � I  � ��e ��d
�e .corecnte****       **** � o   � ��c�c 0 n  �d   � 1   � ��b
�b 
ppgt�g  �f   �  � � � l  �  ��a�` � r   �  � � � m   � ��_�_   � 1   � ��^
�^ 
ppgc�a  �`   �  � � � l 
 ��]�\ � r  
 � � � m   � � � � � , E x p o r t i n g   P l a y l i s t s . . . � 1  	�[
�[ 
ppgd�]  �\   �  � � � l  ��Z�Y � r   � � � m   � � � � � & P r e p a r i n g   e x p o r t . . . � 1  �X
�X 
ppga�Z  �Y   �  � � � l     �W�V�U�W  �V  �U   �  � � � l  ��T�S � r   � � � m  �R�R  � o      �Q�Q 0 c  �T  �S   �  � � � l � ��P�O � X  � ��N � � k  1� � �    r  1J b  1D b  1< b  18	 m  14

 � ( P r o c e s s i n g   p l a y l i s t  	 o  47�M�M 0 c   m  8; �    o f   l <C�L�K I <C�J�I
�J .corecnte****       **** o  <?�H�H 0 n  �I  �L  �K   1  DI�G
�G 
ppga  r  KU I  KQ�F�E�F (0 makefolderinfolder makeFolderInFolder �D o  LM�C�C 0 i  �D  �E   o      �B�B 0 errno errNo  Z  V��A�@ ?  V[ o  VY�?�? 0 errno errNo m  YZ�>�>   k  ^�  r  ^e  m  ^_�=�=    1  _d�<
�< 
ppgt !"! r  fm#$# m  fg�;�;  $ 1  gl�:
�: 
ppgc" %&% r  nw'(' m  nq)) �**  ( 1  qv�9
�9 
ppgd& +,+ r  x�-.- m  x{// �00  . 1  {��8
�8 
ppga, 121 Z  ��34�753 = ��676 o  ���6�6 0 errno errNo7 m  ���5�5 4 I  ���48�3�4 0 popupwarning popUpWarning8 9�29 o  ���1�1 >0 warningtoomanysamenamefolders warningTooManySameNameFolders�2  �3  �7  5 I  ���0:�/�0 0 popupwarning popUpWarning: ;�.; o  ���-�- >0 warningsomethingelsewentwrong warningSomethingElseWentWrong�.  �/  2 <�,< L  ���+�+  �,  �A  �@   =>= O  ��?@? k  ��AA BCB r  ��DED n  ��FGF 2 ���*
�* 
cTrkG 4  ���)H
�) 
cUsPH o  ���(�( 0 i  E o      �'�' 0 t  C IJI r  ��KLK n ��MNM 1  ���&
�& 
txdlN 1  ���%
�% 
ascrL o      �$�$ 0 ap  J OPO r  ��QRQ m  ��SS �TT  :R n     UVU 1  ���#
�# 
txdlV 1  ���"
�" 
ascrP WXW r  ��YZY m  ���!�! Z o      � �  0 s  X [\[ X  �z]�^] k  �u__ `a` r  ��bcb l ��d��d e  ��ee n  ��fgf 1  ���
� 
pLocg o  ���� 0 j  �  �  c o      �� 0 y  a hih O  �kjkj k  �jll mnm Q  �opqo s  �	rsr 4  ��t
� 
filet o  ��� 0 y  s 4  �u
� 
cfolu o  �� 00 currentmusicfolderpath currentMusicFolderPathp R      ���
� .ascrerr ****      � ****�  �  q l �vw�  v $  do nothing, just skip copying   w �xx <   d o   n o t h i n g ,   j u s t   s k i p   c o p y i n gn y�y Z  jz{��z = |}| o  �� *0 retainplaylistorder retainPlaylistOrder} m  �
� boovtrue{ k  f~~ � r  (��� n  $��� 2  $�
� 
citm� l  ���
� c   ��� o  �	�	 0 y  � m  �
� 
ctxt�  �
  � o      �� 0 z  � ��� r  )5��� n  )1��� 4 ,1��
� 
cobj� m  /0����� o  ),�� 0 z  � o      �� 0 f  � ��� r  6J��� b  6F��� b  6B��� n 6>��� I  7>����  0 getcountstring getCountString� �� � o  7:���� 0 s  �   �  �  f  67� m  >A�� ���  _� o  BE���� 0 f  � o      ���� 0 g  � ���� O Kf��� r  Qe��� o  QT���� 0 g  � n      ��� 1  bd��
�� 
pnam� 4  Tb���
�� 
file� l Xa������ b  Xa��� b  X]��� o  XY���� 00 currentmusicfolderpath currentMusicFolderPath� m  Y\�� ���  :� o  ]`���� 0 f  ��  ��  � m  KN���                                                                                  sevs  alis    �  Sixtyten                   �W;�H+   �)System Events.app                                               �VՐ�$        ����  	                CoreServices    �W�      Րg     �) �     9Sixtyten:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    S i x t y t e n  -System/Library/CoreServices/System Events.app   / ��  ��  �  �  �  k m  �����                                                                                  MACS  alis    h  Sixtyten                   �W;�H+   �)
Finder.app                                                      �թ�e        ����  	                CoreServices    �W�      թE     �) �     2Sixtyten:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    S i x t y t e n  &System/Library/CoreServices/Finder.app  / ��  i ���� r  lu��� [  lq��� o  lo���� 0 s  � m  op���� � o      ���� 0 s  ��  � 0 j  ^ o  ������ 0 t  \ ���� r  {���� o  {~���� 0 ap  � n     ��� 1  ����
�� 
txdl� 1  ~���
�� 
ascr��  @ m  �����                                                                                  hook  alis    B  Sixtyten                   �W;�H+     {
iTunes.app                                                     �5�5�d�>        ����  	                Applications    �W�      �d�       {  !Sixtyten:Applications: iTunes.app    
 i T u n e s . a p p    S i x t y t e n  Applications/iTunes.app   / ��  > ��� r  ����� o  ������ 0 c  � 1  ����
�� 
ppgc� ���� r  ����� [  ����� o  ������ 0 c  � m  ������ � o      ���� 0 c  ��  �N 0 i   � o  !���� 0 n  �P  �O   � ��� l     ��������  ��  ��  � ��� l �������� r  ����� o  ������ 0 c  � 1  ����
�� 
ppgc��  ��  � ��� l �������� I �������
�� .sysodelanull��� ��� nmbr� m  ���� ?�      ��  ��  ��  � ��� l �������� r  ����� m  ������  � 1  ����
�� 
ppgt��  ��  � ��� l �������� r  ����� m  ������  � 1  ����
�� 
ppgc��  ��  � ��� l �������� r  ����� m  ���� ���  � 1  ����
�� 
ppgd��  ��  � ��� l �������� r  ����� m  ���� ���  � 1  ����
�� 
ppga��  ��  � ��� l �������� r  ����� I �������
�� .corecnte****       ****� o  ������ 0 n  ��  � o      ���� 0 r  ��  ��  � ��� l �.������ Z  �.������ ?  ����� o  ������ 0 r  � m  ������  � k  ��� ��� Z  ������� = ����� o  ������ 0 r  � m  ������ � r  ����� m  ���� ��� .   p l a y l i s t   w a s   e x p o r t e d .� o      ���� 0 q  ��  � r  �   m  �  � 2   p l a y l i s t s   w e r e   e x p o r t e d . o      ���� 0 q  � �� I ��
�� .sysonotfnull��� ��� TEXT b   l 	����	 c  

 l ���� I ����
�� .corecnte****       **** o  ���� 0 n  ��  ��  ��   m  ��
�� 
TEXT��  ��   o  ���� 0 q   ����
�� 
appr m   �  A l l   D o n e��  ��  ��  � I !.��
�� .sysonotfnull��� ��� TEXT m  !$ � 6 N o   p l a y l i s t s   w e r e   e x p o r t e d . ����
�� 
appr m  '* �  A l l   D o n e . . . ?��  ��  ��  �  l     ��������  ��  ��    i      I      ������ 0 popupwarning popUpWarning �� o      ���� 0 argument  ��  ��   k        !"! O    
#$# I   	������
�� .miscactvnull��� ��� null��  ��  $  f     " %��% I   ��&'
�� .sysodlogaskr        TEXT& o    ���� 0 argument  ' ��()
�� 
btns( J    ** +��+ m    ,, �--  O K��  ) ��.��
�� 
dflt. m    // �00  O K��  ��   121 l     ��������  ��  ��  2 343 i    565 I      �������� 0 getfolderpath getFolderPath��  ��  6 k     #77 898 O     :;: r    <=< l   	>����> n    	?@? 1    	��
�� 
pnam@ l   A����A 1    ��
�� 
sdsk��  ��  ��  ��  = o      ���� 0 sd sD; m     BB�                                                                                  MACS  alis    h  Sixtyten                   �W;�H+   �)
Finder.app                                                      �թ�e        ����  	                CoreServices    �W�      թE     �) �     2Sixtyten:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    S i x t y t e n  &System/Library/CoreServices/Finder.app  / ��  9 CDC O    EFE r    GHG l   I����I n    JKJ 1    ��
�� 
pnamK l   L����L 1    ��
�� 
curu��  ��  ��  ��  H o      ���� 0 un uNF m    MM�                                                                                  sevs  alis    �  Sixtyten                   �W;�H+   �)System Events.app                                               �VՐ�$        ����  	                CoreServices    �W�      Րg     �) �     9Sixtyten:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    S i x t y t e n  -System/Library/CoreServices/System Events.app   / ��  D N��N r    #OPO b    !QRQ b    STS b    UVU o    ���� 0 sd sDV m    WW �XX  : U s e r s :T o    ���� 0 un uNR m     YY �ZZ  : D e s k t o p :P o      ���� 0 
folderpath 
folderPath��  4 [\[ l     ��������  ��  ��  \ ]^] i    _`_ I      ��~�}� 0 
makefolder 
makeFolder�~  �}  ` O     aba k    ~cc ded r    fgf b    hih o    �|�|  0 foldernamebase folderNameBasei n    jkj 1   
 �{
�{ 
shdtk l   
l�z�yl I   
�x�w�v
�x .misccurdldt    ��� null�w  �v  �z  �y  g o      �u�u 0 
foldername 
folderNamee mnm r    opo m    qq �rr  p o      �t�t 0 appendstring appendStringn sts r    uvu m    �s�s  v o      �r�r 0 appendcount appendCountt wxw V    `yzy Q     [{|}{ k   # >~~ � I  # 3�q�p�
�q .corecrel****      � null�p  � �o��
�o 
kocl� m   % &�n
�n 
cfol� �m��
�m 
insh� o   ' (�l�l 0 
folderpath 
folderPath� �k��j
�k 
prdt� K   ) /�� �i��h
�i 
pnam� b   * -��� o   * +�g�g 0 
foldername 
folderName� o   + ,�f�f 0 appendstring appendString�h  �j  � ��� r   4 9��� b   4 7��� o   4 5�e�e 0 
foldername 
folderName� o   5 6�d�d 0 appendstring appendString� o      �c�c 0 
foldername 
folderName� ��� L   : <�� m   : ;�b�b  � ��a�  S   = >�a  | R      �`�_�^
�` .ascrerr ****      � ****�_  �^  } k   F [�� ��� r   F K��� [   F I��� o   F G�]�] 0 appendcount appendCount� m   G H�\�\ � o      �[�[ 0 appendcount appendCount� ��Z� r   L [��� b   L Y��� l  L U��Y�X� c   L U��� b   L Q��� m   L O�� ���    [� o   O P�W�W 0 appendcount appendCount� m   Q T�V
�V 
ctxt�Y  �X  � m   U X�� ���  ]� o      �U�U 0 appendstring appendString�Z  z A    ��� o    �T�T 0 appendcount appendCount� m    �S�S 
x ��R� Z   a ~���Q�� =  a d��� o   a b�P�P 0 appendcount appendCount� m   b c�O�O 
� k   g q�� ��� r   g l��� m   g h�N�N � o      �M�M 0 errornum errorNum� ��L� L   m q�� o   m p�K�K 0 errornum errorNum�L  �Q  � k   t ~�� ��� r   t y��� m   t u�J�J  � o      �I�I 0 errornum errorNum� ��H� L   z ~�� o   z }�G�G 0 errornum errorNum�H  �R  b m     ���                                                                                  MACS  alis    h  Sixtyten                   �W;�H+   �)
Finder.app                                                      �թ�e        ����  	                CoreServices    �W�      թE     �) �     2Sixtyten:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    S i x t y t e n  &System/Library/CoreServices/Finder.app  / ��  ^ ��� l     �F�E�D�F  �E  �D  � ��� i    ��� I      �C��B�C (0 makefolderinfolder makeFolderInFolder� ��A� o      �@�@ 0 nameoffolder nameOfFolder�A  �B  � k     ��� ��� Z    ���?�>� =    ��� l    ��=�<� e     �� n     ��� 7   �;��
�; 
ctxt� m    �:�: � m    
�9�9 � o     �8�8 0 nameoffolder nameOfFolder�=  �<  � m    �� ���  .� r    ��� b    ��� m    �� ���  _� o    �7�7 0 nameoffolder nameOfFolder� o      �6�6 0 nameoffolder nameOfFolder�?  �>  � ��5� O    ���� k    ��� ��� r    "��� o     �4�4 0 nameoffolder nameOfFolder� o      �3�3 "0 musicfoldername musicFolderName� ��� r   # (��� b   # &��� o   # $�2�2 0 
folderpath 
folderPath� o   $ %�1�1 0 
foldername 
folderName� o      �0�0 $0 exportfolderpath exportFolderPath� ��� r   ) ,��� m   ) *�� ���  � o      �/�/ 0 appendstring appendString� ��� r   - 0��� m   - .�.�.  � o      �-�- 0 appendcount appendCount� ��� V   1 ���� Q   9 |���� k   < a�� ��� I  < L�,�+ 
�, .corecrel****      � null�+    �*
�* 
kocl m   > ?�)
�) 
cfol �(
�( 
insh o   @ A�'�' $0 exportfolderpath exportFolderPath �&�%
�& 
prdt K   B H �$�#
�$ 
pnam b   C F	 o   C D�"�" "0 musicfoldername musicFolderName	 o   D E�!�! 0 appendstring appendString�#  �%  � 

 r   M R b   M P o   M N� �  "0 musicfoldername musicFolderName o   N O�� 0 appendstring appendString o      �� "0 musicfoldername musicFolderName  r   S \ b   S X b   S V o   S T�� $0 exportfolderpath exportFolderPath m   T U �  : o   V W�� "0 musicfoldername musicFolderName o      �� 00 currentmusicfolderpath currentMusicFolderPath  L   ] _ m   ] ^��   �  S   ` a�  � R      ���
� .ascrerr ****      � ****�  �  � k   i |   r   i n!"! [   i l#$# o   i j�� 0 appendcount appendCount$ m   j k�� " o      �� 0 appendcount appendCount  %�% r   o |&'& b   o z()( l  o v*��* c   o v+,+ b   o t-.- m   o r// �00    [. o   r s�� 0 appendcount appendCount, m   t u�
� 
ctxt�  �  ) m   v y11 �22  ]' o      �� 0 appendstring appendString�  � A   5 8343 o   5 6�� 0 appendcount appendCount4 m   6 7�� 
� 5�
5 Z   � �67�	86 =  � �9:9 o   � ��� 0 appendcount appendCount: m   � ��� 
7 k   � �;; <=< r   � �>?> m   � ��� ? o      �� 0 errornum errorNum= @�@ L   � �AA o   � ��� 0 errornum errorNum�  �	  8 k   � �BB CDC r   � �EFE m   � ���  F o      �� 0 errornum errorNumD G� G L   � �HH o   � ����� 0 errornum errorNum�   �
  � m    II�                                                                                  MACS  alis    h  Sixtyten                   �W;�H+   �)
Finder.app                                                      �թ�e        ����  	                CoreServices    �W�      թE     �) �     2Sixtyten:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    S i x t y t e n  &System/Library/CoreServices/Finder.app  / ��  �5  � JKJ l     ��������  ��  ��  K L��L i    MNM I      ��O����  0 getcountstring getCountStringO P��P o      ���� 0 	thenumber 	theNumber��  ��  N k     (QQ RSR r     TUT c     VWV o     ���� 0 	thenumber 	theNumberW m    ��
�� 
ctxtU o      ���� 0 cstring cStringS XYX r    Z[Z n    	\]\ 1    	��
�� 
leng] o    ���� 0 cstring cString[ o      ���� 0 c  Y ^��^ Z    (_`��a_ A    bcb o    ���� 0 c  c m    ���� ` L    #dd b    "efe l    g����g e     hh n     iji 7   ��kl
�� 
ctxtk m    ���� l l   m����m \    non m    ���� o o    ���� 0 c  ��  ��  j m    pp �qq  0 0 0 0��  ��  f o     !���� 0 cstring cString��  a L   & (rr o   & '���� 0 cstring cString��  ��       ��stuvwxy��  s �������������� 0 popupwarning popUpWarning�� 0 getfolderpath getFolderPath�� 0 
makefolder 
makeFolder�� (0 makefolderinfolder makeFolderInFolder��  0 getcountstring getCountString
�� .aevtoappnull  �   � ****t ������z{���� 0 popupwarning popUpWarning�� ��|�� |  ���� 0 argument  ��  z ���� 0 argument  { ����,��/����
�� .miscactvnull��� ��� null
�� 
btns
�� 
dflt�� 
�� .sysodlogaskr        TEXT�� ) *j  UO���kv��� u ��6����}~���� 0 getfolderpath getFolderPath��  ��  } ������ 0 sd sD�� 0 un uN~ B����M��WY��
�� 
sdsk
�� 
pnam
�� 
curu�� 0 
folderpath 
folderPath�� $� 	*�,�,E�UO� 	*�,�,E�UO��%�%�%E�v ��`��������� 0 
makefolder 
makeFolder��  ��   ������ 0 appendstring appendString�� 0 appendcount appendCount� ���������q������������������������������  0 foldernamebase folderNameBase
�� .misccurdldt    ��� null
�� 
shdt�� 0 
foldername 
folderName�� 

�� 
kocl
�� 
cfol
�� 
insh�� 0 
folderpath 
folderPath
�� 
prdt
�� 
pnam�� 
�� .corecrel****      � null��  ��  
�� 
ctxt�� 0 errornum errorNum�� �� |�*j �,%E�O�E�OjE�O Gh��  *������Ġ%l� OĠ%E�OjOW X  �kE�Oa �%a &a %E�[OY��O��  kE` O_ Y jE` O_ Uw ������������� (0 makefolderinfolder makeFolderInFolder�� ����� �  ���� 0 nameoffolder nameOfFolder��  � ������������ 0 nameoffolder nameOfFolder�� "0 musicfoldername musicFolderName�� $0 exportfolderpath exportFolderPath�� 0 appendstring appendString�� 0 appendcount appendCount� ����I���������������������������/1��
�� 
ctxt�� 0 
folderpath 
folderPath�� 0 
foldername 
folderName�� 

�� 
kocl
�� 
cfol
�� 
insh
�� 
prdt
�� 
pnam�� 
�� .corecrel****      � null�� 00 currentmusicfolderpath currentMusicFolderPath��  ��  �� 0 errornum errorNum�� ��[�\[Zk\Zk2E�  
�%E�Y hO� ��E�O��%E�O�E�OjE�O Oh�� **����졣%l� O��%E�O��%�%E` OjOW X  �kE�Oa �%�&a %E�[OY��O��  kE` O_ Y jE` O_ Ux ��N����������  0 getcountstring getCountString�� ����� �  ���� 0 	thenumber 	theNumber��  � �������� 0 	thenumber 	theNumber�� 0 cstring cString�� 0 c  � ������p
�� 
ctxt
�� 
leng�� �� )��&E�O��,E�O�� �[�\[Zk\Z�2E�%Y �y �����������
�� .aevtoappnull  �   � ****� k    .��   ��  '��  .��  5��  <��  C��  J��  S��  ���  ���  ���  ���  ���  ���  ���  ���  ���  ���  ���  ���  ��� ��� ��� ��� ��� ��� ��� ��� �����  ��  ��  � ������ 0 i  �� 0 j  � ` %�� ,��� :�~ A�} H�| O�{ ��z�y��x�w�v�u r�t�s y�r }�q�p�o�n�m�l � ��k � ��j ��i�h�g ��f�e�d�c�b ��a ��`�_�^�]
�\)/�[�Z�Y�X�WS�V�U�T��S�R�Q�P�O�N�M�L�K��J����I���H��G�F��  0 foldernamebase folderNameBase�� 00 currentmusicfolderpath currentMusicFolderPath� *0 retainplaylistorder retainPlaylistOrder�~ (0 warningnoselection warningNoSelection�} .0 warningtoomanyfolders warningTooManyFolders�| >0 warningtoomanysamenamefolders warningTooManySameNameFolders�{ >0 warningsomethingelsewentwrong warningSomethingElseWentWrong
�z 
cUsP
�y 
pnam�  
�x 
pSpK
�w eSpKkNon�v 0 m  
�u 
prmp
�t .corecnte****       ****
�s 
TEXT
�r 
appr
�q 
mlsl�p 
�o .gtqpchltns    @   @ ns  �n 0 n  �m 0 popupwarning popUpWarning
�l .miscactvnull��� ��� null
�k 
btns
�j 
dflt
�i .sysodlogaskr        TEXT
�h 
bhit�g 0 b  �f 0 getfolderpath getFolderPath�e 0 
makefolder 
makeFolder�d 0 errno errNo
�c 
ppgt
�b 
ppgc
�a 
ppgd
�` 
ppga�_ 0 c  
�^ 
kocl
�] 
cobj�\ (0 makefolderinfolder makeFolderInFolder
�[ 
cTrk�Z 0 t  
�Y 
ascr
�X 
txdl�W 0 ap  �V 0 s  
�U 
pLoc�T 0 y  
�S 
file
�R 
cfol�Q  �P  
�O 
ctxt
�N 
citm�M 0 z  �L 0 f  �K  0 getcountstring getCountString�J 0 g  
�I .sysodelanull��� ��� nmbr�H 0 r  �G 0 q  
�F .sysonotfnull��� ��� TEXT��/�E�O�E�OfE�O�E�O�E�O�E�O�E�O� I*�-�,a [a ,\Za 81EE` O_ a a _ j a &%a %a a a ea  E` UO_ f  *�k+ OhY hO) *j  UOa !a a "a #a $a %lva &a 'a  (a ),E` *O_ *a +  eE�Y hO*j+ ,O*j+ -E` .O_ .j *�k+ OhY hO_ j *a /,FOj*a 0,FOa 1*a 2,FOa 3*a 4,FOkE` 5O�_ [a 6a 7l kh  a 8_ 5%a 9%_ j %*a 4,FO*�k+ :E` .O_ .j Cj*a /,FOj*a 0,FOa ;*a 2,FOa <*a 4,FO_ .k  *�k+ Y *�k+ OhY hO� �*�/a =-E` >O_ ?a @,E` AOa B_ ?a @,FOkE` CO �_ >[a 6a 7l kh �a D,EE` EOa F u *a G_ E/*a H�/GW X I JhO�e  R_ Ea K&a L-E` MO_ Ma 7i/E` NO)_ Ck+ Oa P%_ N%E` QOa R _ Q*a G�a S%_ N%/�,FUY hUO_ CkE` C[OY�kO_ A_ ?a @,FUO_ 5*a 0,FO_ 5kE` 5[OY��O_ 5*a 0,FOa Tj UOj*a /,FOj*a 0,FOa V*a 2,FOa W*a 4,FO_ j E` XO_ Xj 8_ Xk  a YE` ZY 	a [E` ZO_ j a &_ Z%a a \l ]Y a ^a a _l ]ascr  ��ޭ